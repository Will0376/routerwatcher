package ru.will0376.RW;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import ru.will0376.RW.controllers.Controller;

import java.io.File;

public class Main extends Application {
	public static Scene scene;
	public static Stage ps;
	public static Main instance;
	public static boolean debug = true;
	public static File path = new File(System.getProperty("user.home") + File.separator + ".RW");
	public static String ver = "1.0.12(Test)"; //x,y,z <--  x - (rewrite code only with 0), y - (new version api), z - (every commit)

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		instance = this;
		if (!Main.path.exists())
			Main.path.mkdir();
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ru/will0376/RW/fxml/sample.fxml")); //fuck this with fucking gradle...
		Parent root = loader.load();

		primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/ru/will0376/RW/icon/logo64.png")));
		primaryStage.setTitle("RouterWatcher");
		Platform.setImplicitExit(false);
		primaryStage.setScene(new Scene(root, 833, 533));


		primaryStage.setResizable(false);
		primaryStage.show();

		scene = primaryStage.getScene();
		ps = primaryStage;
		if (Controller.instance.json.getDarkMode()) {
			scene.getStylesheets().add(getClass().getClassLoader().getResource("ru/will0376/RW/fxml/dark-theme.css").toString());
			Controller.instance.darkmode_button.setText(Controller.instance.darkmode_button.getText() + " ✓");
			Controller.instance.lastupd.setFill(Color.WHITE);
			Controller.instance.timertime.setFill(Color.WHITE);
		}

	}
}
