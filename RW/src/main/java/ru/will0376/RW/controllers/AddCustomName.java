package ru.will0376.RW.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import ru.will0376.RW.modules.CustomNames;
import ru.will0376.RW.modules.TableHandler;
import ru.will0376.RW.utils.Info;


public class AddCustomName {
	@FXML
	TextField text;
	private Info info;

	public void setName() {
		String temp = "";
		if (text.getText() != null)
			temp = text.getText();
		CustomNames.addToMap(info.getMac(), temp);
		TableHandler.reloadWhitelist();
		Controller.printToLog("Added custom name for " + info.getName());
	}

	public void removeName() {
		CustomNames.removeFromMap(info.getMac());
		TableHandler.reloadWhitelist();
		Controller.printToLog("Removed custom name for " + info.getName());
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info i) {
		info = i;
	}
}
