package ru.will0376.RW.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ru.will0376.RW.Main;
import ru.will0376.RW.modules.*;
import ru.will0376.RW.utils.Info;
import ru.will0376.RW.utils.Tray;

import java.net.URL;
import java.net.URLClassLoader;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.concurrent.Semaphore;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public class Controller implements Initializable {
	public static ArrayList<String> log = new ArrayList<>();
	public static Controller instance;
	public static boolean run = false;
	private static ArrayList<Stage> stages = new ArrayList<>();
	private static int trying = 0;
	private static Thread thread;
	public static Semaphore sem = new Semaphore(1);
	@FXML
	public TableView table;
	@FXML
	public TableColumn<Info, String> whitelist_column;
	@FXML
	public TableColumn<Info, String> customname_column;
	@FXML
	public TableColumn<Info, String> name_column;
	@FXML
	public TableColumn<Info, String> ip_column;
	@FXML
	public TableColumn<Info, String> mac_column;
	@FXML
	public TableColumn<Info, String> type_column;
	@FXML
	public TableColumn<Info, String> time_column;
	@FXML
	public Text lastupd;
	@FXML
	public Text timertime;
	public Json json = new Json();
	public SSH ssh = new SSH();
	public Whitelist whitelist = new Whitelist();
	public CustomNames customNames = new CustomNames();
	@FXML
	public MenuItem darkmode_button;
	@FXML
	TextField ip_port;
	@FXML
	TextField user;
	@FXML
	TextField pass;
	@FXML
	TextField timer;
	@FXML
	MenuItem toggleAC_button;

	public static void printToLog(String text) {
		try {
			sem.acquire();
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			log.add(sdf.format(cal.getTime()) + "->: " + text);
			sem.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void printToErrLog(String text) {
		try {
			sem.acquire();
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			log.add(sdf.format(cal.getTime()) + "(ERROR) -->: " + text);
			sem.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		instance = this;
		printToLog("Init! RW version: " + Main.ver + " build: " + getBuildTime());

		ip_port.setText(json.getIP());
		user.setText(json.getUser());
		pass.setText(json.getPass());
		timer.setText(String.valueOf(json.getTimer()));
		lastupd.setText("");
		new Tray().newTray();
		Whitelist.whitelist_array = whitelist.getWhitelist();
		ScrollPane sp = new ScrollPane(table);
		sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
		if (json.getAutoConnect()) {
			connect();
			toggleAC_button.setText(toggleAC_button.getText() + " ✓");
		}
		gc();
	}

	public void toggleDarkMode() {
		if (!json.getDarkMode()) {
			json.toggleDarkMode();
			darkmode_button.setText(darkmode_button.getText() + " ✓");
			Main.ps.getScene().getStylesheets().add(getClass().getClassLoader().getResource("ru/will0376/RW/fxml/dark-theme.css").toString());
			stages.forEach(e -> e.getScene().getStylesheets().add(getClass().getClassLoader().getResource("ru/will0376/RW/fxml/dark-theme.css").toString()));
			lastupd.setFill(Color.WHITE);
		} else {
			json.toggleDarkMode();
			darkmode_button.setText(darkmode_button.getText().replace(" ✓", ""));
			Main.ps.getScene().getStylesheets().remove(getClass().getClassLoader().getResource("ru/will0376/RW/fxml/dark-theme.css").toString());
			stages.forEach(e -> e.getScene().getStylesheets().remove(getClass().getClassLoader().getResource("ru/will0376/RW/fxml/dark-theme.css").toString()));
			lastupd.setFill(Color.BLACK);
		}
		json.writeCFG();
	}

	public void toggleAC() {
		if (!json.getAutoConnect()) {
			json.toggleAutoConnect();
			toggleAC_button.setText(toggleAC_button.getText() + " ✓");
		} else {
			json.toggleAutoConnect();
			toggleAC_button.setText(toggleAC_button.getText().replace(" ✓", ""));
		}
		json.writeCFG();
	}

	public void addCustomName() {
		if (table.getSelectionModel().getSelectedItem() != null) {
			Info info = getSelected();
			try {
				Object[] text = new Object[]{"CustomName", "Add custom name for device: " + info.getName(), "330", "150"};
				FXMLLoader fxmlLoader = getNewFXMLLoader((String) text[0]);
				Parent root = fxmlLoader.load();
				fxmlLoader.<AddCustomName>getController().setInfo(info);
				setScene(root, text);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void addRemove() {
		if (table.getSelectionModel().getSelectedItem() != null) {
			Info info = getSelected();

			if (Whitelist.whitelist_array.contains(info.getMac())) {
				Controller.printToLog("Removed from Whitelist " + info.getMac());
				Whitelist.whitelist_array.remove(info.getMac());
			} else {
				Controller.printToLog("Added from Whitelist " + info.getMac());
				Whitelist.whitelist_array.add(info.getMac());
			}

			TableHandler.reloadWhitelist();
			Whitelist.getInstance().addToList(Whitelist.whitelist_array);
			Whitelist.getInstance().writeCFG();
		}
	}

	public Info getSelected() {
		TablePosition pos = (TablePosition) table.getSelectionModel().getSelectedCells().get(0);
		return (Info) table.getItems().get(pos.getRow());
	}

	public void connect() {
		run = true;
		if (SSH.ssh != null && SSH.ssh.isConnected()) {
			try {
				killAll();
				printToLog("Disconnected!");
				run = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		printToLog("Trying connect to " + ip_port.getText());
		updateData();
		SSH.connect();
		if (SSH.ssh.isConnected() && SSH.ssh.isAuthenticated()) {
			if (thread != null && thread.isAlive())
				thread.interrupt();
			printToLog("Start updater thread");
			update();
		}
	}

	public void killAll() {
		try {
			run = false;
			if (thread != null) {
				thread.stop();
				thread = null;
			}
			if (SSH.ssh != null && SSH.ssh.isConnected())
				SSH.ssh.disconnect();

			if (SSH.th != null) {
				SSH.th.stop();
				SSH.th = null;
			}
			printToLog("Killed!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized void refresh() {
		if (run)
			new Thread(() -> {
				if (SSH.ssh.isConnected() && SSH.ssh.isAuthenticated()) {
					printToLog("Refresh");
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
					LocalDateTime now = LocalDateTime.now();
					lastupd.setText("Last update: " + dtf.format(now));
					updateData();
					TableHandler.preparing(ssh.getDHCP(), ssh.getARP());
				} else {
					trying = trying + 1;
					if (trying != 5) {
						printToLog("Connection lost. Attempt to recover.");
						if (SSH.th != null && SSH.th.isAlive()) {
							printToLog("Connect sleep");
							while (SSH.th.isAlive()) {
							}
						}
						SSH.connect();
						refresh();
					} else {
						printToLog("Stopped!");
						killAll();
					}
				}
			}).start();
	}

	public void openLogger() {
		openFXML(new Object[]{"Logger", "null", "600", "400"});
	}

	public void openShell() {

	}

	public void exit() {
		System.exit(0);
	}

	private FXMLLoader getNewFXMLLoader(String name) throws Exception {
		return new FXMLLoader(getClass().getClassLoader().getResource("ru/will0376/RW/fxml/" + name + ".fxml"));
	}

	private void setScene(Parent root1, Object[] text) throws Exception {
		Stage stage1 = new Stage();

		if (text.length != 1)
			if (!text[1].equals("null"))
				stage1.setTitle((String) text[1]);
			else
				stage1.setTitle((String) text[0]);

		stage1.setScene(new Scene(root1, Integer.parseInt((String) text[2]), Integer.parseInt((String) text[3])));
		stage1.getIcons().add(new Image(getClass().getResourceAsStream("/ru/will0376/RW/icon/logo64.png")));
		stage1.initOwner(Main.ps);
		stage1.setResizable(false);
		if (Controller.instance.json.getDarkMode()) {
			stage1.getScene().getStylesheets().add(getClass().getClassLoader().getResource("ru/will0376/RW/fxml/dark-theme.css").toString());
		}
		stages.remove(stage1);
		stages.add(stage1);
		stage1.show();

	}

	public void reconnect() {
		printToLog("trying to reconnect!");
		try {
			if (SSH.ssh.isConnected())
				SSH.ssh.disconnect();
			if (SSH.th.isAlive()) {
				SSH.th.stop();
				SSH.th = null;
			}
			this.connect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void openFXML(Object[] text) {
		try {
			FXMLLoader fxmlLoader = getNewFXMLLoader((String) text[0]);
			setScene(fxmlLoader.load(), text);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateData() {
		if (!json.getIP().equals(ip_port.getText()))
			json.setIp(ip_port.getText());

		if (!json.getUser().equals(user.getText()))
			json.setUser(user.getText());

		if (!json.getPass().equals(pass.getText()))
			json.setPass(pass.getText());

		if (!json.getTimer().equals(Double.valueOf(timer.getText()))) {
			json.setTimer(Double.valueOf(timer.getText()));
			printToLog("Saved time: " + timer.getText());
		}
		json.writeCFG();
	}

	private void update() {
		if (thread == null || !thread.isAlive()) {
			thread = new Thread(() -> {
				while (!Thread.currentThread().isInterrupted()) {
					if (run)
						try {
							Thread.sleep((int) (60000 * Double.parseDouble(timer.getText())));
							printToLog("[Timer] Refresh!");
							refresh();
						} catch (InterruptedException ignore) {
						} catch (Exception e) {
							e.printStackTrace();
						}
				}
			});
			thread.start();
		}
	}

	public void gc() {
		new Thread(() -> {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					Thread.sleep(60000 * 3600);
					System.gc();
					printToLog("GC");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	private String getBuildTime() {
		URLClassLoader cl = (URLClassLoader) getClass().getClassLoader();
		try {
			URL url = cl.findResource("META-INF/MANIFEST.MF");
			Manifest manifest = new Manifest(url.openStream());
			Attributes mainAttributes = manifest.getMainAttributes();
			String temp = mainAttributes.getValue("Build-Timestamp");
			if (temp.isEmpty())
				throw new NullPointerException();
			return temp;
		} catch (Exception E) {
			return "null or dev";
		}
	}
}
