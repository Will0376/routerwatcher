package ru.will0376.RW.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

public class Logger implements Initializable {
	public static int lastlenght = 0;
	public static int lastlenght_err = 0;
	private static Thread th;
	private static Logger instance;
	@FXML
	TextArea logger;
	@FXML
	CheckBox autoscroll;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		instance = this;
		autoscroll.setSelected(Controller.instance.json.getAutoScroll());
		try {
			Controller.sem.acquire();
			for (int i = 0; i < Controller.log.size(); i++) {
				lastlenght = i;
				print(Controller.log.get(i));
			}
			updateThread();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Controller.sem.release();
	}

	public void clear() {
		Controller.sem.release(Controller.sem.drainPermits());
		lastlenght = 0;
		lastlenght_err = 0;
		Controller.log.clear();
		logger.setText("");
		//updateThread();
	}

	synchronized void print(String text) {
		Platform.runLater(() -> logger.appendText(text + System.lineSeparator()));
		if (autoscroll.isSelected())
			logger.selectPositionCaret(logger.getLength());

	}

	void testPrint(String text) {
		System.out.println(text);
	}
	protected void updateThread() {
		if (th == null || !th.isAlive()) {
			th = new Thread(() -> {
				try {
					while (!Thread.currentThread().isInterrupted()) {
						Controller.sem.acquire();
						if (lastlenght != (Controller.log.size() - 1)) {
							for (int i = lastlenght + 1; i < Controller.log.size(); i++)
								try {
									lastlenght = i;
									String temp = Controller.log.get(i);
									testPrint(temp);
									print(temp);

								} catch (Exception ignore) {
								}

						}
						Controller.sem.release();
					}
				} catch (Exception e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				}
			});
			th.start();
		}
	}
}
