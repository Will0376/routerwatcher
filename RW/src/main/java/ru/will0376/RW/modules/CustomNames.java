package ru.will0376.RW.modules;

import org.hjson.JsonObject;
import org.hjson.JsonValue;
import org.hjson.Stringify;
import ru.will0376.RW.Main;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;

public class CustomNames {
	public static HashMap<String, String> names = new HashMap<>(); //<mac,CUN>
	public static JsonObject json;
	private static CustomNames instance;
	private static File config = new File(Main.path + File.separator + "CustomNames.json");

	public CustomNames() {
		instance = this;
		checkJson();
		readJson();
		jsonToMap();
	}

	public static void addToMap(String mac, String name) {
		names.put(mac, name);
		mapToJson();
		writeJson();
	}

	public static void removeFromMap(String mac) {
		names.remove(mac);
		mapToJson();
		writeJson();
	}

	public static void mapToJson() {
		JsonObject jo = new JsonObject();
		names.forEach(jo::add);
		json = jo;
	}

	public static void writeJson() {
		try {
			FileWriter writer = new FileWriter(config);
			writer.write(json.toString(Stringify.HJSON));
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jsonToMap() {
		json.forEach(e -> names.put(e.getName(), e.getValue().asString()));
	}

	private void readJson() {
		try {
			String jsonString = JsonValue.readHjson(new FileReader(config.getPath())).toString();
			json = JsonValue.readHjson(jsonString).asObject();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jsonDefault() {
		JsonObject jo = new JsonObject();
		jo.add("ff:ff:ff:ff:ff", "Custom name");
		json = jo;
	}

	private boolean checkJson() {
		try {
			if (!config.exists()) {
				config.createNewFile();
				jsonDefault();
				writeJson();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


}
