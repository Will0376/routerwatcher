package ru.will0376.RW.modules;

import org.hjson.JsonObject;
import org.hjson.JsonValue;
import org.hjson.Stringify;
import ru.will0376.RW.Main;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class Json {
	private static Json instance;
	private File config = new File(Main.path + File.separator + "config.json");
	private JsonObject json;

	public Json() {
		instance = this;
		checkCFG();
		readCFG();
	}

	public static Json getInstance() {
		return instance;
	}

	private void readCFG() {
		try {
			String jsonString = JsonValue.readHjson(new FileReader(config.getPath())).toString();
			json = JsonValue.readHjson(jsonString).asObject();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jsonDefault() {
		JsonObject jo = new JsonObject();
		jo.add("ip", "192.168.1.1:22");
		jo.add("user", "root");
		jo.add("pass", "root");
		jo.add("timer", "0.5");
		jo.add("autoconnect", false);
		jo.add("dmode", false);
		jo.add("autoscroll", true);
		json = jo;
	}

	public void writeCFG() {
		try {
			FileWriter writer = new FileWriter(config);
			writer.write(json.toString(Stringify.HJSON));
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public JsonObject getJson() {
		return json;
	}

	public void setJson(JsonObject jo) {
		json = jo;
	}

	public String getIP() {
		return json.getString("ip", "192.168.1.1:22");
	}

	public String getUser() {
		return json.getString("user", "root");
	}

	public boolean getAutoScroll() {
		return json.getBoolean("autoscroll", true);
	}

	public void setAutoScroll(String autoscroll) {
		json.set("autoscroll", autoscroll);
	}

	public void setUser(String user) {
		json.set("user", user);
	}

	public String getPass() {
		return json.getString("pass", "root");
	}

	public void setPass(String pass) {
		json.set("pass", pass);
	}

	public Double getTimer() {
		return json.getDouble("timer", 60);
	}

	public void setTimer(Double i) {
		json.set("timer", i);
	}

	public void setIp(String ip) {
		json.set("ip", ip);
	}

	public boolean getAutoConnect() {
		return json.get("autoconnect").asBoolean();
	}

	public void toggleAutoConnect() {
		json.set("autoconnect", !json.get("autoconnect").asBoolean());
	}

	public boolean getDarkMode() {
		return json.get("dmode").asBoolean();
	}

	public void toggleDarkMode() {
		json.set("dmode", !json.get("dmode").asBoolean());
	}

	private boolean checkCFG() {
		try {
			if (!Main.path.exists())
				Main.path.mkdir();

			if (!config.exists()) {
				config.createNewFile();
				jsonDefault();
				writeCFG();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
