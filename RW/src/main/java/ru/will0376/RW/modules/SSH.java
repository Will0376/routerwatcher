package ru.will0376.RW.modules;


import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;
import ru.will0376.RW.controllers.Controller;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class SSH {
	public static SSH instance;
	public static SSHClient ssh;
	public static Thread th;

	public SSH() {
		instance = this;
		ssh = new SSHClient();
	}

	public static SSH getInstance() {
		return instance;
	}

	public static void connect() {
		AtomicBoolean ab = new AtomicBoolean(false);
		th = new Thread(() -> {
			try {
				ssh = new SSHClient();
				if (!ssh.isConnected()) {
					ssh.addHostKeyVerifier(new PromiscuousVerifier());
					ssh.connect(Controller.instance.json.getIP().split(":")[0], Integer.parseInt(Controller.instance.json.getIP().split(":")[1]));

					ssh.authPassword(Controller.instance.json.getUser(), Controller.instance.json.getPass());
					ssh.startSession().exec("echo [RW] Connect from " + InetAddress.getLocalHost().getHostAddress() + " > /dev/kmsg");
					Controller.printToLog("Connected to " + Controller.instance.json.getIP());
				}
				ab.set(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Controller.instance.refresh();
		});
		th.start();
		while (!ab.get()) {
		}
	}

	public String getDHCP() {
		if (ssh.isConnected() && ssh.isAuthenticated()) {
			try {
				Session.Command ou = ssh.startSession().exec("cat /tmp/dhcp.leases");
				String text = IOUtils.readFully(ou.getInputStream()).toString();
				ou.join(5, TimeUnit.SECONDS);

				Controller.printToLog("[SSH_DHCP] Data received");
				return text;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Controller.printToErrLog("SSH is not available");
			Controller.instance.reconnect();
			return "";
		}
		return "";
	}

	public String getARP() {
		if (ssh.isConnected() && ssh.isAuthenticated()) {
			try {
				Session.Command ou = ssh.startSession().exec("cat /proc/net/arp");
				String text = IOUtils.readFully(ou.getInputStream()).toString();
				ou.join(5, TimeUnit.SECONDS);

				Controller.printToLog("[SSH_ARP] Data received");
				return text;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Controller.printToErrLog("SSH is not available");
			Controller.instance.reconnect();
			return "";
		}
		return "";
	}
}
