package ru.will0376.RW.modules;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.will0376.RW.controllers.Controller;
import ru.will0376.RW.utils.Info;
import ru.will0376.RW.utils.Tray;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

public class TableHandler {
	public static ArrayList<Info> devices = new ArrayList<>();
	public static ObservableList<Info> usersData = FXCollections.observableArrayList();
	static boolean first = true;
	private static ArrayList<String> offline_devices_mac = new ArrayList<>();

	public static void preparing(String dhcp, String arp) {
		offline_devices_mac.clear();
		if (dhcp.isEmpty() || arp.isEmpty()) {
			Controller.printToErrLog("DHCP or ARP is empty");
			return;
		}
		for (String text : arp.split("\n")) {
			if (text.contains("eth0.2") || text.contains("IP")) continue;
			text = text.replaceAll("\\s+", " ");
			if (text.contains("0x0")) offline_devices_mac.add(text.split(" ")[3]);
		}
		for (String text : dhcp.split("\n")) {
			Date time = Date.from(Instant.ofEpochSecond(Long.parseLong(text.split(" ")[0])));
			String mac = text.split(" ")[1];
			String ip = text.split(" ")[2];
			String name = text.split(" ")[3];
			String type = text.split(" ")[4];
			if (offline_devices_mac.contains(mac)) continue;
			String whitelist = "";
			if (Whitelist.whitelist_array.contains(mac))
				whitelist = "*";

			if (!check(mac)) {
				devices.add(new Info(time.toString(), mac, ip, name, type, whitelist, CustomNames.names.getOrDefault(mac, name)));
				if (!first) {
					Tray.sendToTray("New device!", "Local IP: " + ip + System.lineSeparator() + "with mac: " + mac + System.lineSeparator() + "with name: " + CustomNames.names.getOrDefault(mac, name));
				}
			}
		}

		if (first)
			first = false;
		else
			deleteDisabled();

		reloadWhitelist();
	}

	private synchronized static void deleteDisabled() {
		for (int ii = 0; ii < devices.size(); ii++) {
			Info i = devices.get(ii);
			if (offline_devices_mac.contains(i.getMac())) {
				devices.remove(ii);
				Tray.sendToTray("Disconnected device!", "Local IP: " + i.getIp() + System.lineSeparator() + "with mac: " + i.getMac() + System.lineSeparator() + "with name: " + CustomNames.names.getOrDefault(i.getMac(), i.getName()));
			}
		}
	}

	private static boolean check(String mac) {
		AtomicBoolean ab = new AtomicBoolean(false);
		devices.forEach(info -> {
			if (info.compare(mac)) ab.set(true);
		});
		return ab.get();
	}

	private static void setUsersData() {
		usersData.clear();
		usersData.addAll(devices);
	}

	public static void reloadWhitelist() {
		devices.forEach(e -> {
			if (Whitelist.whitelist_array.contains(e.getMac()))
				e.setWhitelist("*");
			else
				e.setWhitelist("");
		});
		devices.forEach(e -> e.setCustomname(CustomNames.names.getOrDefault(e.getMac(), "")));
		setUsersData();
		addAllToTable();
	}

	public static void addAllToTable() {
		Platform.runLater(() -> {
			Controller c = Controller.instance;
			c.table.setItems(FXCollections.observableArrayList(""));

			c.whitelist_column.setCellValueFactory(new PropertyValueFactory<>("whitelist"));
			c.name_column.setCellValueFactory(new PropertyValueFactory<>("name"));
			c.ip_column.setCellValueFactory(new PropertyValueFactory<>("ip"));
			c.customname_column.setCellValueFactory(new PropertyValueFactory<>("customname"));
			c.mac_column.setCellValueFactory(new PropertyValueFactory<>("mac"));
			c.type_column.setCellValueFactory(new PropertyValueFactory<>("type"));
			c.time_column.setCellValueFactory(new PropertyValueFactory<>("time"));

			c.table.setItems(usersData);
			c.table.getSortOrder().add(c.ip_column);
		});
	}
}
