package ru.will0376.RW.modules;

import org.hjson.JsonArray;
import org.hjson.JsonObject;
import org.hjson.JsonValue;
import org.hjson.Stringify;
import ru.will0376.RW.Main;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class Whitelist {
	public static ArrayList<String> whitelist_array = new ArrayList<>();
	private static Whitelist instance;
	private File config = new File(Main.path + File.separator + "whitelist.json");
	private JsonObject json;

	public Whitelist() {
		instance = this;
		checkCFG();
		readCFG();
	}

	public static Whitelist getInstance() {
		return instance;
	}

	private void readCFG() {
		try {
			String jsonString = JsonValue.readHjson(new FileReader(config.getPath())).toString();
			json = JsonValue.readHjson(jsonString).asObject();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addToList(ArrayList<String> al) {
		JsonObject jo = new JsonObject();
		JsonArray ja = new JsonArray();
		al.forEach(ja::add);
		jo.add("list", ja);
		json = jo;
	}

	private void jsonDefault() {
		JsonObject jo = new JsonObject();
		JsonArray ja = new JsonArray();
		ja.add("ff:ff:ff:ff:ff");
		jo.add("list", ja);
		json = jo;
	}

	public ArrayList<String> getWhitelist() {
		JsonArray ja = json.get("list").asArray();
		ArrayList<String> al = new ArrayList<>();
		ja.forEach(e -> al.add(e.asString()));
		return al;
	}

	public void writeCFG() {
		try {
			FileWriter writer = new FileWriter(config);
			writer.write(json.toString(Stringify.HJSON));
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkCFG() {
		try {
			if (!config.exists()) {
				config.createNewFile();
				jsonDefault();
				writeCFG();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
