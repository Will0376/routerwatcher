package ru.will0376.RW.utils;

public class Info {
	private String Whitelist;
	private String Time;
	private String Mac;
	private String Ip;
	private String Name;
	private String Type;
	private String Customname;

	public Info(String time, String mac, String ip, String name, String type, String whitelist, String customname) {
		this.Time = time;
		this.Mac = mac;
		this.Ip = ip;
		this.Name = name;
		this.Type = type;
		this.Whitelist = whitelist;
		this.Customname = customname;
	}

	public boolean compare(String Mac) {
		if (this.Mac != null)
			return this.Mac.equals(Mac);
		else
			return false;
	}

	public String getMac() {
		return Mac;
	}

	public void setMac(String mac) {
		Mac = mac;
	}

	public String getIp() {
		return Ip;
	}

	public void setIp(String ip) {
		Ip = ip;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getTime() {
		return Time;
	}

	public void setTime(String time) {
		Time = time;
	}

	public String getWhitelist() {
		return Whitelist;
	}

	public void setWhitelist(String whitelist) {
		Whitelist = whitelist;
	}

	public String getCustomname() {
		return Customname;
	}

	public void setCustomname(String customname) {
		Customname = customname;
	}
}
