package ru.will0376.RW.utils;

import javafx.application.Platform;
import ru.will0376.RW.Main;
import ru.will0376.RW.controllers.Controller;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

public class Tray {
	public static Tray instance;
	public static TrayIcon trayIcon;

	public static void sendToTray(String[] text) {
		sendToTray(text[0], text[1]);
	}

	public static void sendToTray(String head, String body) {
		try {
			Controller.printToLog(head + "\n" + body);
			if (trayIcon != null)
				trayIcon.displayMessage(head, body, TrayIcon.MessageType.INFO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void newTray() {
		try {
			instance = this;
			if (!SystemTray.isSupported()) {
				System.out.println("System tray is not supported !!! ");
				return;
			}

			BufferedImage trayIconImage = ImageIO.read(getClass().getResourceAsStream("/ru/will0376/RW/icon/logo64.png"));
			PopupMenu trayPopupMenu = new PopupMenu();

			MenuItem action = new MenuItem("Show");
			action.addActionListener(e -> {
				if (Main.ps.isShowing())
					Platform.runLater(() -> Main.ps.close());
				else
					Platform.runLater(() -> Main.ps.show());
			});
			trayPopupMenu.add(action);

			MenuItem refresh = new MenuItem("Refresh");
			refresh.addActionListener(e -> Controller.instance.refresh());
			trayPopupMenu.add(refresh);

			MenuItem close = new MenuItem("Close");
			close.addActionListener(e -> System.exit(0));
			trayPopupMenu.add(close);

			trayIcon = new TrayIcon(trayIconImage, "RouterWatcher", trayPopupMenu);

			trayIcon.setImageAutoSize(true);

			trayIcon.addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getButton() == 1) {
						if (Main.ps.isShowing())
							Platform.runLater(() -> Main.ps.close());
						else
							Platform.runLater(() -> Main.ps.show());
					}
				}

				@Override
				public void mousePressed(MouseEvent e) {

				}

				@Override
				public void mouseReleased(MouseEvent e) {

				}

				@Override
				public void mouseEntered(MouseEvent e) {

				}

				@Override
				public void mouseExited(MouseEvent e) {

				}
			});
			SystemTray systemTray = SystemTray.getSystemTray();
			systemTray.add(trayIcon);
		} catch (Exception awtException) {
			awtException.printStackTrace();
		}
	}
}
